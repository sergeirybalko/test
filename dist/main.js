/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);

var _canvas = __webpack_require__(2);

var _canvas2 = _interopRequireDefault(_canvas);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var can = new _canvas2.default();

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Canvas = function () {
    function Canvas(w, h, colors, lineWidths) {
        _classCallCheck(this, Canvas);

        this._canvas = document.getElementById('canvas');
        this._canvas.width = w || window.innerWidth - window.innerWidth / 100 * 10;
        this._canvas.height = h || window.innerHeight - window.innerHeight / 100 * 10;
        this._ctx = this._canvas.getContext('2d');
        this._colors = colors || ['red', 'green', 'blue', 'yellow', 'black', 'orange', 'pink'];
        this._selectColor = document.getElementById('line-color');
        this._lineWidths = lineWidths || [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this._selectLine = document.getElementById('line-width');
        this._clearBtn = document.getElementById('clear');
        this._undoBtn = document.getElementById('undo');
        this._redoBtn = document.getElementById('redo');
        this._saveBtn = document.getElementById('save');
        this._readBtn = document.getElementById('read');
        this._arrPath = [];
        this._itemPath = [];
        this._tempPath = [];
        this._x = 0;
        this._y = 0;
        this._prevX = 0;
        this._prevY = 0;
        this._isDraw = false;
        this.init();
    }

    _createClass(Canvas, [{
        key: 'init',
        value: function init() {
            var _this = this;

            this._canvas.addEventListener('mousedown', function (e) {
                _this.eventHandler('down', e);
            });
            this._canvas.addEventListener('mouseup', function (e) {
                _this.eventHandler('up', e);
            });
            this._canvas.addEventListener('mousemove', function (e) {
                _this.eventHandler('move', e);
            });
            this._clearBtn.addEventListener('click', function () {
                _this.clear();
            });
            this._undoBtn.addEventListener('click', function () {
                if (_this._arrPath.length !== 0) {
                    var itemPath = _this._arrPath.pop();
                    _this._tempPath.push(itemPath);
                    _this.clear();
                    if (_this._arrPath.length !== 0) {
                        var _iteratorNormalCompletion = true;
                        var _didIteratorError = false;
                        var _iteratorError = undefined;

                        try {
                            for (var _iterator = _this._arrPath[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                var subArray = _step.value;
                                var _iteratorNormalCompletion2 = true;
                                var _didIteratorError2 = false;
                                var _iteratorError2 = undefined;

                                try {
                                    for (var _iterator2 = subArray[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                        var item = _step2.value;

                                        _this.draw(item.color, item.lineWidth, item.prevX, item.prevY, item.x, item.y);
                                    }
                                } catch (err) {
                                    _didIteratorError2 = true;
                                    _iteratorError2 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                            _iterator2.return();
                                        }
                                    } finally {
                                        if (_didIteratorError2) {
                                            throw _iteratorError2;
                                        }
                                    }
                                }
                            }
                        } catch (err) {
                            _didIteratorError = true;
                            _iteratorError = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }
                            } finally {
                                if (_didIteratorError) {
                                    throw _iteratorError;
                                }
                            }
                        }
                    }
                }
            });
            this._redoBtn.addEventListener('click', function () {
                if (_this._tempPath.length !== 0) {
                    var itemPath = _this._tempPath.pop();
                    _this._arrPath.push(itemPath);
                    _this.clear();
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = _this._arrPath[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var subArray = _step3.value;
                            var _iteratorNormalCompletion4 = true;
                            var _didIteratorError4 = false;
                            var _iteratorError4 = undefined;

                            try {
                                for (var _iterator4 = subArray[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                    var item = _step4.value;

                                    _this.draw(item.color, item.lineWidth, item.prevX, item.prevY, item.x, item.y);
                                }
                            } catch (err) {
                                _didIteratorError4 = true;
                                _iteratorError4 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                        _iterator4.return();
                                    }
                                } finally {
                                    if (_didIteratorError4) {
                                        throw _iteratorError4;
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }
                }
            });
            this._saveBtn.addEventListener('click', function () {
                _this.save();
            });
            this._readBtn.addEventListener('click', function () {
                _this.read();
            });
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = this._colors[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var color = _step5.value;

                    var option = document.createElement('option');
                    option.value = color;
                    option.innerHTML = color;
                    this._selectColor.appendChild(option);
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }

            var _iteratorNormalCompletion6 = true;
            var _didIteratorError6 = false;
            var _iteratorError6 = undefined;

            try {
                for (var _iterator6 = this._lineWidths[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                    var line = _step6.value;

                    var option = document.createElement('option');
                    option.value = line;
                    option.innerHTML = line;
                    this._selectLine.appendChild(option);
                }
            } catch (err) {
                _didIteratorError6 = true;
                _iteratorError6 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion6 && _iterator6.return) {
                        _iterator6.return();
                    }
                } finally {
                    if (_didIteratorError6) {
                        throw _iteratorError6;
                    }
                }
            }
        }
    }, {
        key: 'clear',
        value: function clear() {
            var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            var w = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this._canvas.width;
            var h = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : this._canvas.height;

            this._ctx.clearRect(x, y, w, h);
        }
    }, {
        key: 'draw',
        value: function draw(color, lineWidth, prevX, prevY, x, y) {
            this._ctx.beginPath();
            this._ctx.lineWidth = lineWidth;
            this._ctx.strokeStyle = color;
            this._ctx.moveTo(prevX, prevY);
            this._ctx.lineTo(x, y);
            this._ctx.stroke();
            this._ctx.closePath();
        }
    }, {
        key: 'save',
        value: function save() {
            var url = this._canvas.toDataURL();
            localStorage.setItem('imgData', url);
        }
    }, {
        key: 'read',
        value: function read() {
            var url = localStorage.getItem('imgData');
            var img = document.createElement('img');
            img.setAttribute('src', url);
            document.getElementsByClassName('container')[0].appendChild(img);
        }
    }, {
        key: 'eventHandler',
        value: function eventHandler(eventMouse, e) {
            switch (eventMouse) {
                case 'down':
                    {
                        this._prevX = this._x;
                        this._prevY = this._y;
                        this._x = e.clientX - this._canvas.offsetLeft;
                        this._y = e.clientY - this._canvas.offsetTop;
                        if (this._selectColor.selectedIndex !== 0 && this._selectLine.selectedIndex !== 0) {
                            this._isDraw = true;
                        }
                    }
                    break;
                case 'up':
                    {
                        this._isDraw = false;
                        this._arrPath.push(this._itemPath);
                        this._itemPath = [];
                    }
                    break;
                case 'move':
                    {
                        if (this._isDraw) {
                            var color = this._selectColor.options[this._selectColor.selectedIndex].value;
                            var lineWidth = this._selectLine.options[this._selectLine.selectedIndex].value;

                            this._prevX = this._x;
                            this._prevY = this._y;
                            this._x = e.clientX - this._canvas.offsetLeft;
                            this._y = e.clientY - this._canvas.offsetTop;

                            this._itemPath.push({
                                prevX: this._prevX,
                                prevY: this._prevY,
                                x: this._x,
                                y: this._y,
                                color: color,
                                lineWidth: lineWidth
                            });

                            this.draw(color, lineWidth, this._prevX, this._prevY, this._x, this._y);
                        }
                    }
                    break;
            }
        }
    }]);

    return Canvas;
}();

var canvas = new Canvas();

/***/ })
/******/ ]);