const path = require('path'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    CleanWebpackPlugin = require('clean-webpack-plugin');


const extractSass = new ExtractTextPlugin({
    filename: "./style.css",
    disable: process.env.NODE_ENV === "dev"
});

module.exports = {
    entry: './src/js/global.js',
    devServer: {
        contentBase: './dist'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.(html)$/,
                use: ['file-loader?name=[path][name].[ext]']
            },
            {
                test: /\.scss$/,
                include: [path.resolve(__dirname, 'src/css')],
                use: extractSass.extract({
                    use: [
                        {
                            loader: "css-loader",
                        },
                        {
                            loader: "sass-loader"
                        }
                    ]
                }),
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], {
            exclude: path.resolve(__dirname, 'dist/index.html'),
        }),
        extractSass,
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
};