class Canvas {
    constructor(w, h, colors, lineWidths) {
        this._canvas = document.getElementById('canvas');
        this._canvas.width = w || window.innerWidth - ((window.innerWidth / 100) * 10);
        this._canvas.height = h || window.innerHeight - ((window.innerHeight / 100) * 10);
        this._ctx = this._canvas.getContext('2d');
        this._colors = colors || ['red', 'green', 'blue', 'yellow', 'black', 'orange', 'pink'];
        this._selectColor = document.getElementById('line-color');
        this._lineWidths = lineWidths || [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this._selectLine = document.getElementById('line-width');
        this._clearBtn = document.getElementById('clear');
        this._undoBtn = document.getElementById('undo');
        this._redoBtn = document.getElementById('redo');
        this._saveBtn = document.getElementById('save');
        this._readBtn = document.getElementById('read');
        this._arrPath = [];
        this._itemPath = [];
        this._tempPath = [];
        this._x = 0;
        this._y = 0;
        this._prevX = 0;
        this._prevY = 0;
        this._isDraw = false;
        this.init();
    }

    init() {
        this._canvas.addEventListener('mousedown', e => {
            this.eventHandler('down', e);
        });
        this._canvas.addEventListener('mouseup', e => {
            this.eventHandler('up', e);
        });
        this._canvas.addEventListener('mousemove', e => {
            this.eventHandler('move', e);
        });
        this._clearBtn.addEventListener('click', () => {
            this.clear();
        });
        this._undoBtn.addEventListener('click', () => {
            if (this._arrPath.length !== 0) {
                let itemPath = this._arrPath.pop();
                this._tempPath.push(itemPath);
                this.clear();
                if (this._arrPath.length !== 0) {
                    for (let subArray of this._arrPath) {
                        for (let item of subArray) {
                            this.draw(item.color, item.lineWidth, item.prevX, item.prevY, item.x, item.y);
                        }
                    }
                }
            }
        });
        this._redoBtn.addEventListener('click', () => {
            if (this._tempPath.length !== 0) {
                let itemPath = this._tempPath.pop();
                this._arrPath.push(itemPath);
                this.clear();
                for (let subArray of this._arrPath) {
                    for (let item of subArray) {
                        this.draw(item.color, item.lineWidth, item.prevX, item.prevY, item.x, item.y);
                    }
                }
            }
        });
        this._saveBtn.addEventListener('click', () => {
            this.save();
        });
        this._readBtn.addEventListener('click', () => {
            this.read();
        });
        for (let color of this._colors) {
            const option = document.createElement('option');
            option.value = color;
            option.innerHTML = color;
            this._selectColor.appendChild(option);
        }
        for (let line of this._lineWidths) {
            const option = document.createElement('option');
            option.value = line;
            option.innerHTML = line;
            this._selectLine.appendChild(option);
        }
    }

    clear(x = 0, y = 0, w = this._canvas.width, h = this._canvas.height) {
        this._ctx.clearRect(x, y, w, h);
    }

    draw(color, lineWidth, prevX, prevY, x, y) {
        this._ctx.beginPath();
        this._ctx.lineWidth = lineWidth;
        this._ctx.strokeStyle = color;
        this._ctx.moveTo(prevX, prevY);
        this._ctx.lineTo(x, y);
        this._ctx.stroke();
        this._ctx.closePath();
    }

    save() {
        let url = this._canvas.toDataURL();
        localStorage.setItem('imgData', url);
    }

    read() {
        let url = localStorage.getItem('imgData');
        let img = document.createElement('img');
        img.setAttribute('src', url);
        document.getElementsByClassName('container')[0].appendChild(img);
    }

    eventHandler(eventMouse, e) {
        switch (eventMouse) {
            case 'down': {
                this._prevX = this._x;
                this._prevY = this._y;
                this._x = e.clientX - this._canvas.offsetLeft;
                this._y = e.clientY - this._canvas.offsetTop;
                if (this._selectColor.selectedIndex !== 0 && this._selectLine.selectedIndex !== 0) {
                    this._isDraw = true;
                }
            }
                break;
            case 'up': {
                this._isDraw = false;
                this._arrPath.push(this._itemPath);
                this._itemPath = [];
            }
                break;
            case 'move': {
                if (this._isDraw) {
                    const color = this._selectColor.options[this._selectColor.selectedIndex].value;
                    const lineWidth = this._selectLine.options[this._selectLine.selectedIndex].value;

                    this._prevX = this._x;
                    this._prevY = this._y;
                    this._x = e.clientX - this._canvas.offsetLeft;
                    this._y = e.clientY - this._canvas.offsetTop;

                    this._itemPath.push({
                        prevX: this._prevX,
                        prevY: this._prevY,
                        x: this._x,
                        y: this._y,
                        color: color,
                        lineWidth: lineWidth
                    });

                    this.draw(color, lineWidth, this._prevX, this._prevY, this._x, this._y);
                }
            }
                break;
        }
    }
}

const canvas = new Canvas();